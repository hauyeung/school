from __future__ import unicode_literals

from django.db import models

# Create your models here.
class school(models.Model):
    name = models.CharField(max_length=1000)

class classroom (models.Model):
    classroom = models.OneToOneField(school)
    name = models.CharField(max_length=1000)

class student(models.Model):
    classroom = models.OneToOneField(classroom)
    name = models.CharField(max_length=1000)
